package com.branco.android.files.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.branco.android.files.R;
import com.branco.android.files.ui.fragment.FilesFragment;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private FilesFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment = new FilesFragment();
        getFragmentManager().beginTransaction().add(R.id.fragment_frame, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (fragment.onBackPressed()){
            super.onBackPressed();
        }
    }


}

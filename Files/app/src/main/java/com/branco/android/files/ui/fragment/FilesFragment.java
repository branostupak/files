package com.branco.android.files.ui.fragment;

import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.branco.android.files.R;
import com.branco.android.files.async.LoadDirTask;
import com.branco.android.files.dialog.ConfirmationDialog;
import com.branco.android.files.general.Utils;
import com.branco.android.files.ui.adapter.FileAdapter;
import com.branco.android.files.ui.adapter.MultiSelectionManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FilesFragment extends Fragment implements ConfirmationDialog.ConfirmationDialogInterface{
    public static final String TAG = FilesFragment.class.getSimpleName();
    //    public String dir = Environment.getExternalStorageDirectory().getPath();
    public static final String DEFAULT_DIR = "/storage";
    public String current_dir;

    public ActionMode actionMode;
    private FileAdapter fileAdapter;
    private List<File> list = new ArrayList<>();

    @BindView(R.id.path_element) TextView pathElement;
    @BindView(R.id.list_view) RecyclerView recyclerView;
    @BindView(R.id.swipe_files) SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_files, container, false);
        ButterKnife.bind(this, view);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 6));
        } else {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
        fileAdapter = new FileAdapter(getActivity(), list);
        recyclerView.setAdapter(fileAdapter);

        getDir(DEFAULT_DIR);
        setClickListeners();
        setSwipeRefreshLayout();

        return view;
    }

    public void setSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDir(current_dir);
                swipeRefreshLayout.setRefreshing(false);    // fake it
            }
        });
    }


    public void setClickListeners() {
        fileAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (actionMode != null)
                    actionMode.finish();

                int position = (int) view.getTag();
                File file = list.get(position);

                if (file.isDirectory()) {
                    getDir(file.getPath());
                } else {
                    MimeTypeMap myMime = MimeTypeMap.getSingleton();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    String mimeType = myMime.getMimeTypeFromExtension(Utils.getFileExtension(file));
                    intent.setDataAndType(Uri.fromFile(file),mimeType);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(getActivity(), getString(R.string.cant_open_file), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        fileAdapter.setOnItemLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                int position = (int) view.getTag();
                File file = list.get(position);

                getSelectionManager().toggleSelected(fileAdapter.getItem(position));
                onSelectionChanged();
                fileAdapter.notifyItemChanged(position);
                return true;
            }
        });
    }

    private void getDir(String dirPath) {
        current_dir = dirPath;
        pathElement.setText(dirPath);

        new LoadDirTask() {
            @Override
            protected void onPostExecute(List<File> updatedFiles) {
                super.onPostExecute(updatedFiles);
                list.clear();
                list.addAll(updatedFiles);
                fileAdapter.notifyDataSetChanged();
            }
        }.execute(dirPath);
    }

    public boolean onBackPressed() {
        if (current_dir.equals(DEFAULT_DIR)) {
            return true;
        } else {
            String history = current_dir.substring(0, current_dir.lastIndexOf('/'));
            getDir(history);
            return false;
        }
    }


    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater menuInflater = getActivity().getMenuInflater();
            menuInflater.inflate(R.menu.context_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.context_menu_delete:
                    showDeleteAlertDialog();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            clearSelection();
            notifyDataSetChanged();
            actionMode = null;
        }
    };


    private MultiSelectionManager getSelectionManager() {
        return fileAdapter.getSelectionManager();
    }

    private void notifyDataSetChanged() {
        fileAdapter.notifyDataSetChanged();
    }

    private void removeItems(List<File> items) {
        for (File file : items) {
            try {
                if (file.isDirectory())
                    deleteDirectory(file);
                else
                    file.delete();

                fileAdapter.getList().remove(file);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    static public boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    private void onSelectionChanged() {
        List selectedItems = getSelectionManager().getSelectedItems();

        if (selectedItems.isEmpty()) {
            if (actionMode != null) {
                actionMode.finish();
            }
        } else {
            if (actionMode == null) {
                actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(actionModeCallback);
            }
            actionMode.setTitle(getString(R.string.selected_count, selectedItems.size()));
        }
    }

    private List<File> getSelectedItems() {
        return getSelectionManager().getSelectedItems();
    }

    private void clearSelection() {
        getSelectionManager().clearSelection();
    }

    private void showDeleteAlertDialog(){
        ConfirmationDialog dialog = ConfirmationDialog.newInstance(R.string.delete_alert, R.string.delete, R.string.cancel);
        dialog.setConfirmationDialogInterface(this);
        dialog.show(getFragmentManager(), TAG);
    }

    @Override
    public void positiveMethod() {
        removeItems(getSelectedItems());
        clearSelection();
        onSelectionChanged();
        notifyDataSetChanged();
    }

    @Override
    public void negativeMethod() { }

}

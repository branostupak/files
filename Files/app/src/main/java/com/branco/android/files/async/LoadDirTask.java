package com.branco.android.files.async;

import android.os.AsyncTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LoadDirTask extends AsyncTask<String, Void, List<File>> {
    @Override
    protected List<File> doInBackground(String... dirPath) {
        File file = new File(dirPath[0]);
        List<File> temp = new ArrayList<>();

        try {
            for (File item: file.listFiles()) {
                if (!item.isHidden() && item.canRead())
                    temp.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }
}

package com.branco.android.files.ui.adapter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



public class MultiSelectionManager<T> {
    private Set<T> selectedItems;


    public MultiSelectionManager() {
        selectedItems = new HashSet<T>();
    }

    public List<T> getSelectedItems() {
        return new ArrayList<>(selectedItems);
    }

    public void setSelected(T item, boolean selected) {
        if (selected) {
            selectedItems.add(item);
        } else {
            selectedItems.remove(item);
        }
    }

    public boolean getSelected(T item) {
        return selectedItems.contains(item);
    }

    public void clearSelection() {
        selectedItems.clear();
    }

    public boolean isSelectable(T item) {
        return true;
    }

    public void toggleSelected(T item) {
        setSelected(item, !getSelected(item));
    }

    public void toggleSelectedList(List<T> list) {
        for (T item : list) {
            setSelected(item, !getSelected(item));
        }
    }

    public void removeItem(T item) {
        setSelected(item, false);
    }

    public int size() {
        return selectedItems.size();
    }


}

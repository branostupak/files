package com.branco.android.files.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

public class ConfirmationDialog extends DialogFragment {
    protected static final String MESSAGE = "message";
    protected static final String POSITIVE_BUTTON = "positive";
    protected static final String NEGATIVE_BUTTON = "negative";

    private ConfirmationDialogInterface confirmationDialogInterface;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getArguments().getInt(MESSAGE))
                .setPositiveButton(getArguments().getInt(POSITIVE_BUTTON), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        confirmationDialogInterface. positiveMethod();
                    }
                })
                .setNegativeButton(getArguments().getInt(NEGATIVE_BUTTON), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        confirmationDialogInterface.negativeMethod();
                    }
                });
        return builder.create();
    }


    public static ConfirmationDialog newInstance(int message, int positiveButton, int negativeButton) {
        ConfirmationDialog confirmationDialog = new ConfirmationDialog();

        Bundle bundle = new Bundle();
        bundle.putInt(MESSAGE, message);
        bundle.putInt(POSITIVE_BUTTON, positiveButton);
        bundle.putInt(NEGATIVE_BUTTON, negativeButton);
        confirmationDialog.setArguments(bundle);
        return confirmationDialog;
    }

    public void setConfirmationDialogInterface(ConfirmationDialogInterface confirmationDialogInterface) {
        this.confirmationDialogInterface = confirmationDialogInterface;
    }

    public interface ConfirmationDialogInterface {
        void positiveMethod();
        void negativeMethod();
    }
}
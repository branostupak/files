package com.branco.android.files.general;

import com.branco.android.files.R;

import java.io.File;
import java.text.DecimalFormat;


public class Utils {

    public static String bytesToSizeReadable(long size) {
        DecimalFormat df = new DecimalFormat("0.00");

        float sizeKB = 1024.0f;
        float sizeMB = sizeKB * sizeKB;
        float sizeGB = sizeMB * sizeKB;

        if(size < sizeMB)
            return df.format(size / sizeKB)+ " KB";
        else if(size < sizeGB)
            return df.format(size / sizeMB) + " MB";
        else
            return df.format(size / sizeGB) + " GB";
    }

    public static String getFileExtension(File file) {
        if (file.isFile()) {
            return file.getName().substring(file.getName().lastIndexOf('.') + 1);
        }
        return "";
    }

    public static int getIconResourceByFileExtension(String extension) {
        switch (extension.toLowerCase()) {
            case "apk":
                return R.drawable.apk;
            case "pdf":
                return R.drawable.pdf;
            case "html":
            case "css":
            case "js":
                return R.drawable.html;
            case "aac":
            case "mp3":
            case "ogg":
            case "flac":
                return R.drawable.music;
            case "jpg":
            case "jpeg":
            case "png":
            case "gif":
                return R.drawable.image;
            case "mkv":
            case "avi":
            case "mp4":
            case "3gp":
                return R.drawable.video;
            case "txt":
                return R.drawable.txt;
            case "rar":
            case "zip":
                return R.drawable.archive;
            case "":
                return R.drawable.folder;
            default:
                return R.drawable.unknown;
        }
    }
}

package com.branco.android.files.ui.adapter;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.branco.android.files.R;
import com.branco.android.files.general.Utils;

import java.io.File;
import java.util.List;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {
    public static final String TAG = FileAdapter.class.getSimpleName();

    private List<File> list;
    private Context context;
    protected MultiSelectionManager selectionManager;
    View.OnClickListener clickListener;
    View.OnLongClickListener longClickListener;

    public FileAdapter(Context context, List<File> list) {
        this.list = list;
        this.context = context;
        this.selectionManager = new MultiSelectionManager();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_element, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        File item = list.get(position);

        int resource = Utils.getIconResourceByFileExtension(Utils.getFileExtension(item));
        holder.icon.setImageResource(resource);
        holder.name.setText(item.getName());
        holder.detail.setVisibility(View.GONE);
        holder.itemView.setTag(position);

        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT && item.isFile()) {
            // show details of files in portrait
            holder.detail.setText(Utils.bytesToSizeReadable(item.length()));
            holder.detail.setVisibility(View.VISIBLE);
        }

        if (selectionManager.getSelected(item)) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.selected_list_item));
        } else {
            holder.itemView.setBackgroundColor(Color.WHITE);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(View.OnClickListener callback) {
        clickListener = callback;
    }
    public void setOnItemLongClickListener(View.OnLongClickListener callback) {
        longClickListener = callback;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.item_icon) ImageView icon;
        @BindView(R.id.item_name) TextView name;
        @BindView(R.id.item_detail) TextView detail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onClick(view);
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    longClickListener.onLongClick(view);
                    return true;
                }
            });
        }
    }

    public MultiSelectionManager getSelectionManager() {
        return selectionManager;
    }


    public File getItem(int position) {
        return list.get(position);
    }

    public List<File> getList() {
        return list;
    }

}